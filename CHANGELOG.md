

## [0.1.3](https://gitlab.com/everlabs/react-internal-course/compare/0.1.2...0.1.3) (2023-11-11)


### Bug Fixes

* update release-it config ([0064f9c](https://gitlab.com/everlabs/react-internal-course/commit/0064f9c2f8b7b1bb82ff7366b21da256cadbf033))